// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueYouTubeEmbed from 'vue-youtube-embed'
import VueMatchHeights from 'vue-match-heights'
import VueMaterialAdapter from 'vue-material-adapter'
import VModal from 'vue-js-modal'
import './theme.scss'
import App from './App'
import router from './router'

Vue.config.productionTip = false

Vue.use(VueMaterialAdapter)
Vue.use(VueYouTubeEmbed, { global: true, componentId: 'youtube-media' })
Vue.use(VueMatchHeights, {
  disabled: [600] // Disable for Small breakpoint
})
Vue.use(VModal, {
  dynamic: true,
  dialog: true,
  adaptive: true,
  resizeable: true,
  injectModalsContainer: true
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
